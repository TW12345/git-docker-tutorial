# README #

This repository contains material for the Git + Docker tutorial at the JGI, December 2016

### What do I do? ###

First go to the 'git' directory and work through the lessons in order,
then go to the 'docker' directory and do the same.